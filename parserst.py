#!/usr/bin/env python3

# 1C txt parser for golovast wages calculating system
# Copyright (C) 2017 Anton Karmanov

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import configparser
import sys
import csv
import os


tsv_filename = 'input.txt'
ini_filename = 'worktime_lists.lst'
list_charger = list()
employee = ''

args = sys.argv

if len(args) > 1:
    tsv_filename = args[1]
if len(args) > 2:
    ini_filename = args[2]

if not os.path.exists(tsv_filename):
    print('Файл {} не найден!\n'.format(tsv_filename))
    print('Укажите корректное имя файла в качестве первого аргумента\n' +
          'или дайте вашему файлу имя {}'.format(tsv_filename) +
          ' и повторите комманду.\n')
    print('Выполнение отменено.')
    sys.exit(2)


class Time_list:
    '''
    Not actually a class, but a structure, that keeps some data of worktime.
    '''
    def __init__(self, name, work_type, time, company, trip):
        self.name = name
        self.work_type = work_type
        self.time = time.replace(',', '.')
        self.company = company
        self.trip = trip


def trip_parser(trip):
    '''
    Aux function that helps to count outer works.
    '''
    if trip.isdigit():
        return(int(trip))
    else:
        return(0)


def parse_tsv(employee):
    '''
    Simple parser for 1C txt output files.
    It makes list of objects with properties of worktime lists.
    '''
    with open(tsv_filename, 'r', encoding='utf-8') as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter='\t')
        preprev_row = ['']
        prev_row = ['']
        for row in tsv_in:
            for item in row:
                if employee == '' \
                   and item.find('Ссылка.Сотрудник Равно') != -1:
                    employee = item.split(' ')[2][1:]

            if preprev_row[0].find('Лист учета') == -1 \
               and prev_row[0].find('Лист учета') == -1 \
               and row[0].find('Лист учета') != -1:
                work_type = preprev_row[0]
            if prev_row[0].find('Лист учета') == -1 \
               and row[0].find('Лист учета') != -1:
                company = prev_row[0]
            if row[0].find('Лист учета') != -1:
                list_charger.append(
                    Time_list(
                        row[0][28:], work_type, row[1],
                        company, trip_parser(row[3])))
            preprev_row = prev_row
            prev_row = row
    return(employee)


def save_ini_data(ini_filename):
    '''
    Save data to ini-file.
    Sections are worktime lists names and values are lists properties.
    '''
    ini = configparser.ConfigParser(default_section='Итог')
    i = 1
    for time_list in list_charger:
        section = str(i) + '—' + time_list.name
        ini.add_section(section)
        ini.set(section, 'Вид работ', time_list.work_type)
        ini.set(section, 'Потраченное время', str(time_list.time))
        ini.set(section, 'Контрагент', time_list.company)
        ini.set(section, 'Выезды', str(time_list.trip))
        i += 1

    ini.set('Итог', 'Сотрудник', str(employee))

    with open(ini_filename, 'w') as ini_file:
        ini.write(ini_file)


if __name__ == '__main__':

    employee = parse_tsv(employee)
    save_ini_data(ini_filename)

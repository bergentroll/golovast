#!/usr/bin/env python3

# It is a part of the  golovast wages calculating system
# Copyright (C) 2017 Anton Karmanov

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Добавить разбор ini листов

import configparser
import datetime
import shutil
import sys
import os

employee = ''
post = 'Системный администратор'
total_worktime = 0
trips_num = 0
total_plan = 0
target_plan = 80000
rate_of_pay = 10000
month = ''
year = ''
lists_file = 'worktime_lists.lst'
output_filename = lists_file
list_charger = list()
price_filename = 'price.ini'
price_charger = dict()
interactive = True
communication_compensation = 100
trip_wage = 40  # компенсация за каждую поездку.
trip_to_plan = 240  # в план за каждую поездку.


class Time_list:
    '''
    Class presents worktime lists.
    Every list contains data about only one kind of works.
    '''

    def __init__(self, name, work_type, time, company, trip):
        self.name = name
        self.work_type = work_type
        self.time = float(time)
        self.company = company
        self.trip = int(trip)
        self.plan = 0
        self.price = 0

    def set_price(self, price, trip_extra, nonworking_price, kind):
        self.kind = kind
        if self.work_type.find('изучение решения') == -1:
            self.price = float(price)
        self.trip_extra = float(trip_extra)
        self.price_nonworking = float(nonworking_price)

    def calculate_plan(self):
        '''
        Calculate payment based on time and tax information.
        '''
        if self.kind == 'разовая':
            self.plan = self.price
        elif (self.kind == 'почасовая' and
              self.work_type.find('Не рабочее') != -1):
            self.plan = self.time * self.price_nonworking
        elif self.kind == 'почасовая':
            self.plan = self.time * (self.price + self.trip_extra)
        else:
            print('Неизвестный тип: ', self.kind, '!')
            print('Выход из метода.')
            return(0)


class Price():
    '''
    Structure contains tax data.
    '''
    def __init__(
            self, price, trip_extra, nonworking_price, kind):
        self.price = price
        self.trip_extra = trip_extra
        self.nonworking_price = nonworking_price
        self.kind = kind


def number(s):
    '''
    Check is argument a number.
    '''
    try:
        float(s)
        return True
    except ValueError:
        return False


def read_worktime_lists(lists_file, employee):
    '''
    Gets data from worktime lists data file.
    '''
    if not os.path.exists(lists_file):
        print('Файл ', lists_file, ' не найден!')
        print('Выход из процедуры.')
        return(0)
    lists = configparser.ConfigParser(default_section='Итог')
    lists.read(lists_file)
    lists_list = lists.sections()
    for item in lists_list:
        list_charger.append(
            Time_list(
                item,
                lists.get(item, 'вид работ'),
                lists.get(item, 'потраченное время'),
                lists.get(item, 'контрагент'),
                lists.get(item, 'выезды')))

    if employee == '':
        employee = lists.get('Итог', 'сотрудник')
    return(employee)


def read_price(price_filename):
    '''
    Get data from price ini-file.
    '''
    if not os.path.exists(price_filename):
        print('Файл ', price_filename, ' не найден!')
        print('Выход из процедуры.')
        return(0)
    price_list = configparser.ConfigParser(default_section='По умолчанию')
    price_list.read(price_filename, encoding='utf-8')
    items_list = price_list.sections()
    items_list.append('По умолчанию')
    for item in items_list:
        price = Price(
            price_list.get(item, 'цена'),
            price_list.get(item, 'надбавка за выезд'),
            price_list.get(item, 'цена нерабочее'),
            price_list.get(item, 'тип оплаты'))
        price_charger[item] = price


def dialog(item, list_lenght, i):
    '''
    Interactive user dialog.
    '''
    item.calculate_plan()
    while True:
        counter = '—[ {} из {} ]'.format(i, list_lenght)
        print(counter + (80 - len(counter)) * '—')
        print('Лист рабочего времени {}\n'.format(item.name) +
              'Контрагент: {}\n'.format(item.company) +
              'Вид работ: {}\n'.format(item.work_type) +
              'Вид оплаты: {}\n'.format(item.kind) +
              'Часов потрачено: {:.1f}\n'.format(item.time) +
              'Стоимость: {:.2f} руб\n'.format(item.price) +
              'В план: {:.2f} руб'.format(item.plan))
        print(10 * '—')
        print('1 — Подтвердить\n' +
              '2 — Изменить стоимость\n' +
              '3 — Изменить вид оплаты\n' +
              'f — Пропустить диалог для оставшихся {} листов\n'.format(
                  list_lenght - i))
        answer = input('Введите номер пункта меню: ')
        if answer == '1':
            print('Подтверждение принято.' + 3 * '\n')
            return(True)
        elif answer == '2':
            new_price = input('Введите новую сумму: ')
            if number(new_price):
                item.price = float(new_price)
                item.calculate_plan()
            else:
                print('"{}" не является числом!'.format(new_price) +
                      3 * '\n')
        elif answer == '3':
            print('\n' +
                  '1 — Почасовая оплата\n' +
                  '2 — Разовая оплата\n')
            answer = input('Введите номер пункта меню: ')
            if answer == '1':
                item.kind = 'почасовая'
                item.calculate_plan()
                print('Вид оплаты изменен на почасовую.' + 3 * '\n')
            elif answer == '2':
                item.kind = 'разовая'
                item.calculate_plan()
                print('Вид оплаты изменен на разовую.' + 3 * '\n')
            else:
                print('"{}" не является корректным вводом!'.format(answer) +
                      3 * '\n')
        elif answer == 'f':
            print('Принято. Пропускаю диалог для оставшихся листов!' +
                  3 * '\n')
            return(False)
        else:
            print('"{}" не является корректным вводом!'.format(answer) +
                  3 * '\n')


def interactive_mode():
    '''
    Sets payments data to worktime lists and call interactive dialog
    that provides user to change some values.
    '''
    list_length = len(list_charger)
    i = 1
    local_interactive = True
    for item in list_charger:
        if item.company in price_charger:
            pointer = item.company
        else:
            pointer = 'По умолчанию'
        item.set_price(
            price_charger[pointer].price,
            price_charger[pointer].trip_extra,
            price_charger[pointer].nonworking_price,
            price_charger[pointer].kind)
        if interactive and local_interactive:
            local_interactive = dialog(item, list_length, i)
        else:
            item.calculate_plan()
        i += 1


def calculate_total():
    '''
    Count total values.
    '''
    total_plan, trips_num, total_worktime = 0, 0, 0
    for item in list_charger:
        total_plan += item.plan
        trips_num += item.trip
        total_worktime += item.time
    total_plan += trip_to_plan * trips_num
    contribution = communication_compensation + trips_num * trip_wage
    print(3 * '\n' + 80 * '=')
    print('Всего план за месяц: {:.2f} руб\n'.format(total_plan) +
          'Всего выездов за месяц: {}\n'.format(trips_num) +
          'Всего часов выработано за месяц: {:.1f}\n'.format(total_worktime))
    return(total_plan, trips_num, total_worktime, contribution)


def save_ini_data():
    '''
    Save data to ini-file.
    Sections are worktime lists names and values are lists properties.
    '''
    lists = configparser.ConfigParser(default_section='Итог')
    lists.read(output_filename, encoding='utf-8')
    for time_list in list_charger:
        section = time_list.name
        lists.set(section, 'Вид оплаты', str(time_list.kind))
        lists.set(section, 'Цена', str(time_list.price))
        lists.set(section, 'Надбавка за выезд', str(time_list.trip_extra))
        lists.set(section, 'Цена нерабочее', str(time_list.price_nonworking))
        lists.set(section, 'В план', str(time_list.plan))
    lists.set('Итог', 'Сотрудник', str(employee))
    lists.set('Итог', 'Месяц', str(month))
    lists.set('Итог', 'Год', str(year))
    lists.set('Итог', 'Должность', post)
    lists.set('Итог', 'Всего в план', str(total_plan))
    lists.set('Итог', 'План', str(target_plan))
    lists.set('Итог', 'Всего часов', str(round(total_worktime, 1)))
    lists.set('Итог', 'Всего выездов', str(trips_num))
    lists.set('Итог', 'Размер оклада', str(rate_of_pay))
    lists.set('Итог', 'Компенсация за выезды и связь', str(contribution))
    with open(output_filename, 'w') as ini_file:
        lists.write(ini_file)
        print('Новые данные внесены в {}'.format(output_filename))


if __name__ == "__main__":
    args = sys.argv

    if len(args) > 1:
        lists_file = args[1]
    if len(args) > 2:
        output_filename = args[2]

    if not os.path.exists(lists_file):
        print('Файл {} не найден!\n'.format(lists_file))
        print('Укажите корректное имя файла в качестве первого аргумента\n' +
              'или дайте вашему файлу имя {}'.format(lists_file) +
              ' и повторите комманду.\n')
        print('Выполнение отменено.')
        sys.exit(2)

    if lists_file != output_filename:
        print('Файл {} будет перезаписан!'.format(output_filename))
        answer = input('Продолжить? (Y/N) ')
        if answer.upper() != 'Y':
            print('Выполнение отменено.')
            sys.exit(1)
        else:
            print(3 * '\n')
            shutil.copyfile(lists_file, output_filename)

    if month == '':
        month = datetime.date.today().strftime('%B')
    if year == '':
        year = str(datetime.date.today().year)
    employee = read_worktime_lists(lists_file, employee)
    read_price(price_filename)
    interactive_mode()
    total_plan, trips_num, total_worktime, contribution = calculate_total()
    save_ini_data()

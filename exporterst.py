#!/usr/bin/env python3

# Exporter is a part of the golovast wages calculating system
# Copyright (C) 2017 Anton Karmanov

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import configparser
import zipfile
import shutil
import sys
import os

args = sys.argv
path = os.path.dirname(os.path.realpath(__file__))
lists_file = 'worktime_lists.lst'
template = os.path.abspath(path + '/template/template.odf')
export = 'подбивка.odf'
varcharger = dict()
percent_to_wage = 30  # сколько процентов от плана идет в зарплату


def get_variables(lists_file):
    '''
    Function loads variables from worklists data file.
    '''
    if not os.path.exists(lists_file):
        print('Файл ', lists_file, ' не найден!')
        print('Выход из процедуры.')
        return(0)
    lists = configparser.ConfigParser()
    lists.read(lists_file)
    varcharger['month'] = lists.get('Итог', 'месяц')
    varcharger['year'] = lists.get('Итог', 'год')
    varcharger['lastname'] = lists.get('Итог', 'сотрудник')
    varcharger['post'] = lists.get('Итог', 'должность')
    varcharger['plantotal'] = lists.getfloat('Итог', 'всего в план')
    varcharger['target_plan'] = lists.getfloat('Итог', 'план')
    varcharger['rate_of_pay'] = lists.getfloat('Итог', 'размер оклада')
    varcharger['total_worktime'] = lists.getfloat('Итог', 'всего часов')
    varcharger['trips_num'] = lists.get('Итог', 'всего выездов')
    varcharger['contribution'] = lists.getfloat(
        'Итог', 'компенсация за выезды и связь')
    varcharger['plan_percent'] = (
        varcharger['plantotal'] * 100 / varcharger['target_plan'])
    varcharger['total'] = (
        varcharger['plantotal'] * percent_to_wage / 100 +
        varcharger['contribution'])
    for key in varcharger:
        value = varcharger[key]
        if isinstance(value, float):
            varcharger[key] = '{0:.2f}'.format(value)
        else:
            varcharger[key] = str(value)


def process_template(content):
    '''
    Replaces labels in content file to values.
    '''
    for key in varcharger:
        content[1] = content[1].replace('$' + key + '$', varcharger[key])
    return(content)


def main(template, lists_file, export):
    '''
    The main function operates files and call other functions.
    '''

    if len(args) > 1:
        lists_file = args[1]
    if len(args) > 2:
        export = args[2]

    if not os.path.exists(lists_file):
        print('Файл {} не найден!\n'.format(lists_file))
        print('Укажите корректное имя файла в качестве первого аргумента\n' +
              'или дайте вашему файлу имя {}'.format(lists_file) +
              ' и повторите комманду.\n')
        print('Выполнение отменено.')
        sys.exit(2)

    get_variables(lists_file)
    template_zip = zipfile.ZipFile(template, 'r')
    template_zip.extractall(path + '/tmp')
    template_zip.close()
    content_xml = open(path + '/tmp/content.xml', 'r', -1, encoding='utf-8')

    content = content_xml.readlines()
    content_xml.close()
    content = process_template(content)

    content_xml = open(path + '/tmp/content.xml', 'w')
    [content_xml.write("{}\n".format(line)) for line in content]
    content_xml.close()
    shutil.make_archive(export, 'zip', path + '/tmp')
    shutil.rmtree(path + '/tmp')
    if os.path.exists(export):
        os.remove(export)
    os.rename(export + '.zip', export)
    print('\nФайл "{}" создан.'.format(export))


if __name__ == "__main__":
    main(template, lists_file, export)

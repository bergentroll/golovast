#!/usr/bin/env python3

# Scripts collection for parsing exectly kind of 1C txt files
# Copyright (C) 2017 Anton Karmanov

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os

input_filename = 'input.txt'
output_filename = 'подбивка.odf'
args = sys.argv

if len(args) > 1:
    input_filename = args[1]
if len(args) > 2:
    output_filename = args[2]

if os.system('python3 parserst.py {}'.format(input_filename)):
    sys.exit()
if os.system('python3 calcst.py '):
    sys.exit()
if os.system(
        'python3 exporterst.py worktime_lists.lst {}'
        .format(output_filename)):
    sys.exit()
